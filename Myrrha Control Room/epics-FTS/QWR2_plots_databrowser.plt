<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title></title>
    <save_changes>true</save_changes>
    <show_legend>false</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>false</grid>
    <scroll>true</scroll>
    <update_period>3.0</update_period>
    <scroll_step>5</scroll_step>
    <start>-59.17 sec</start>
    <end>now</end>
    <archive_rescale>NONE</archive_rescale>
    <foreground>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
    </foreground>
    <background>
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
    </background>
    <title_font>Liberation Sans|20|1</title_font>
    <label_font>Liberation Sans|14|1</label_font>
    <scale_font>Liberation Sans|12|0</scale_font>
    <legend_font>Liberation Sans|14|0</legend_font>
    <axes>
        <axis>
            <visible>true</visible>
            <name>Value 3</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>-180.0</min>
            <max>180.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 4</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>-5.0</min>
            <max>5.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 5</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>25.0</min>
            <max>75.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>Delta Phi</display_name>
            <visible>true</visible>
            <name>QWR2:PHASE_CORRECTED_PV</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.5</period>
            <ring_size>1200</ring_size>
            <request>RAW</request>
        </pv>
        <pv>
            <display_name>Delta Phi (details)</display_name>
            <visible>true</visible>
            <name>QWR2:PHASE_CORRECTED_PV</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.5</period>
            <ring_size>1200</ring_size>
            <request>RAW</request>
        </pv>
        <pv>
            <display_name>Motor position</display_name>
            <visible>true</visible>
            <name>QWR2:DISTANCE_PV</name>
            <axis>2</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>127</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.5</period>
            <ring_size>1200</ring_size>
            <request>RAW</request>
        </pv>
    </pvlist>
</databrowser>
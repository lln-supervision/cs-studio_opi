
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
#logger = ScriptUtil.getLogger()
#logger.info("Start")

children = widget.getDisplayModel().runtimeChildren()

name_widget1 = children.getChildByName("coolingView")
button1 = children.getChildByName("Action Button_1")
button2 = children.getChildByName("Action Button_2")
button3 = children.getChildByName("Action Button_3")
buttonselected = children.getChildByName("Text Update")
name_pv = ScriptUtil.getPrimaryPV(buttonselected)
numero = PVUtil.getLong(name_pv)
#logger.info(str(numero))
num=int(numero)

if num==1:
	button1.setPropertyValue("enabled", False) 
else:
	button1.setPropertyValue("enabled", True)

if num==2:
	button2.setPropertyValue("enabled", False)
else:
	button2.setPropertyValue("enabled", True)
	
if num==3:
	button3.setPropertyValue("enabled", False)
else:
	button3.setPropertyValue("enabled", True)
	

class Switcher(object):
          def indirect(self,i):
                   method_name='number_'+str(i)
                   method=getattr(self,method_name,lambda :'Invalid')
                   return method()
          def number_0(self):
                   return 'Accueil.bob'
          def number_1(self):       			
                   return 'Cooling_01_LEBT1.bob'
          def number_2(self):
                   return 'Cooling_02_MEBT.bob'
          def number_3(self):
                   return 'Cooling_03_TOPBUNKER.bob'
          def number_4(self):
                   return 'Cooling_04_FS-T.bob'

view=Switcher()          
#logger.info(view.indirect(num))       
name_widget1.setPropertyValue("file", view.indirect(num))


// N.B. : https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/prog_guide/javascript.html

// <IMPORT>
importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);
importPackage(Packages.java.lang);
importPackage(Packages.org.eclipse.jface.dialogs);
// </IMPORT>

// <Global Vars>

	//Font Color definition to show issues on the object
var RED = ColorFontUtil.RED;	
var COLOR = ColorFontUtil.getColorFromRGB (240,240,240); //Standard grey
	
var pv = widget.getPV(); 	// Get the PV set in the Basic-PV_NAME of the object used to lauch this present script
var pulse = [0, 1, 0 ];		// Shape of the pulse
var TimeAfterWrite = 100;	// TimeSleep Value after Writing PV
var TimeAfterLoop = 50;		// TimeSleep after Readback failed attempt
// </Global Vars>


// <Thread>
runnable = {	
	run:function()
		{	
			var result=-1;										//result of PV ReadBack
			widget.setPropertyValue("background_color",COLOR); 	//Used to reset the default color if become RED (error)
			widget.setPropertyValue("enabled",false); 			// Widget not usable when the Thread is running
			for(var i=0; i < pulse.length; i++)  				// Browse all the 'Shape of the pulse' char
			{
				PVUtil.writePV(pv,pulse[i]);					//Write to the PV initialized at the beginning
				Thread.sleep(TimeAfterWrite);					//Force a time sleep to avoid CSS to fail! mandatory when using the CA
				result=AckChange(pulse[i]);						//Status of the PV Write ReadBack
			    if (result!=1)									// If Fail
			    {
			    	ConsoleUtil.writeError("Error while sending the command. Retry or check the CA"); 	//Print to console
			    	widget.setPropertyValue("background_color",RED);									//Change the widget Color
			    	break;																				//Exit of the loop
			    }							
			}			
			widget.setPropertyValue("enabled",true);			// end of the thread function : Widget available again
		}	
	};		
new Thread(new Runnable(runnable)).start();
// </Thread>



// <Functions>
function AckChange(val) 		//get the value to check for ReadBack
{
	var i = 0;
	NbreEssais=5; 				// Number of attemps
   	var PvRead=-1;				// Readback value to be compared with the Set value
   	   		
	while (i<NbreEssais) 
	{
		PvRead = PVUtil.getDouble(pv);
		if (PvRead == val)
		{
			 status=1;
			 break;
		}
		status=0;
		Thread.sleep(TimeAfterLoop*(i+1)); // Incrementing time sleep
		i++;		
	}
	//ConsoleUtil.writeError("status : ");
	return status;
}
// </Functions>
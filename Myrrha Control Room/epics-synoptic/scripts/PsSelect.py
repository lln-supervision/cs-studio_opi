from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil, ValueUtil
from org.csstudio.display.builder.model.macros import Macros
import os

logger = ScriptUtil.getLogger()

#logger.info("PWD : " + os.getcwd())

interrupt = PVUtil.getString(pvs[0])
descriptor = PVUtil.getString(pvs[1])

#GetMacro=widget.getEffectiveMacros()
#logger.info("DEVICE_TEST:" + GetMacro.getValue("DEVICE"))

#NewMacro=Macros()
#NewMacro.add("DEVICE", GetMacro.getValue("DEVICE"))
#logger.info("\nnew:" + str(NewMacro))


if interrupt == "1" :
	if  "JEMA" in descriptor :
		logger.info("WE HAVE A JEMA HERE")
		ScriptUtil.openDisplay(widget, "/Myrrha Control Room/epics-magnet-PS_V2/OPI/MagnetPS_JEMA.bob","STANDALONE",dict())

	elif "CAENELS" in descriptor :
		logger.info("WE HAVE A CAENELS HERE")
		ScriptUtil.openDisplay(widget, "/Myrrha Control Room/epics-magnet-PS_V2/OPI/EasyDriver.bob", "STANDALONE",dict())
	else :
		logger.info("BAD REQUEST")
		ScriptUtil.showErrorDialog(widget, "bad PS manufacturer")

	
	
	
	

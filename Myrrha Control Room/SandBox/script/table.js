importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);
importPackage(Packages.java.lang);
importPackage(Packages.org.eclipse.jface.dialogs);


var LineNumb = 5;
var ColumnNumb = 4;
var OpiToTable = "./OPI/table.opi";
var OpiColumn = "./OPI/column.opi";
var OpiLine = "./OPI/line.opi";
var X_Offset = 100;
var Y_Offset = 50;
var X_OPI=220;
var Y_OPI=55;
var MacroName="DEVICE";

heading(ColumnNumb,LineNumb);

var k = 0;
for (var i=0;i<LineNumb;i++)
{		 
	for (var j=0;j<ColumnNumb;j++)
	{
			var linkingContainer = WidgetUtil.createWidgetModel("org.csstudio.opibuilder.widgets.linkingContainer");	
			linkingContainer.setPropertyValue("opi_file",OpiToTable);
			linkingContainer.setPropertyValue("auto_size", true);
			linkingContainer.setPropertyValue("zoom_to_fit", false);
			linkingContainer.setPropertyValue("border_style", 1);
			linkingContainer.setPropertyValue("x", j*X_OPI + X_Offset);
			linkingContainer.setPropertyValue("y", i*Y_OPI+ Y_Offset);
			if (k<10)
			{
				linkingContainer.addMacro(MacroName, "0".concat(k));
			}
			else
			{
				linkingContainer.addMacro(MacroName,k);
			}
			widget.addChild(linkingContainer);
			k++;
	}
}


// <Functions>

function heading (column, line) 		
{
	for ( var i=0; i<column;i++)
	{
			var linkingContainer = WidgetUtil.createWidgetModel("org.csstudio.opibuilder.widgets.linkingContainer");	
			linkingContainer.setPropertyValue("opi_file",OpiColumn);
			linkingContainer.setPropertyValue("auto_size", true);
			linkingContainer.setPropertyValue("zoom_to_fit", false);
			linkingContainer.setPropertyValue("border_style", 1);
			linkingContainer.setPropertyValue("x", i*X_OPI + X_Offset);
			linkingContainer.setPropertyValue("y", 0);	
			linkingContainer.addMacro("TEXT",i);
			widget.addChild(linkingContainer);
	}
	
	for ( var i=0; i<line;i++)
	{
			var linkingContainer = WidgetUtil.createWidgetModel("org.csstudio.opibuilder.widgets.linkingContainer");	
			linkingContainer.setPropertyValue("opi_file",OpiLine);
			linkingContainer.setPropertyValue("auto_size", true);
			linkingContainer.setPropertyValue("zoom_to_fit", false);
			linkingContainer.setPropertyValue("border_style", 1);
			linkingContainer.setPropertyValue("x", 0);
			linkingContainer.setPropertyValue("y", i*Y_OPI + Y_Offset);	
			linkingContainer.addMacro("TEXT",i*column);
			widget.addChild(linkingContainer);
	}	
}
// </Functions>






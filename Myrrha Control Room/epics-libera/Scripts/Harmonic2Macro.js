importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);
var pv0 = PVUtil.getString(pvArray[0]);
macroInput.put("F", pv0);
widgetController.setPropertyValue("macros", macroInput);


from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

#logger = ScriptUtil.getLogger()
#logger.info("Start")

children = widget.getDisplayModel().runtimeChildren()
name_widget = children.getChildByName("BooleanButton")

#logger.info("Name widget " + str(name_widget))

name_pv = ScriptUtil.getPrimaryPV(name_widget)

#logger.info("Name PV " + str(name_pv))

value = PVUtil.getLong(name_pv)

#logger.info("value="+str(value))

if value == 0:
	widget.setPropertyValue("confirm_message", "Are your sure you want turn ON this gauge?")

#logger.info("ON")

else:
	widget.setPropertyValue("confirm_message", "Are your sure you want turn OFF this gauge?")
	
#logger.info("OFF")
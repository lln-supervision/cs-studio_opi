
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
#logger = ScriptUtil.getLogger()
#logger.info("Start")

children = widget.getDisplayModel().runtimeChildren()
name_widget1 = children.getChildByName("BooleanButton1")
name_widget2 = children.getChildByName("BooleanButton2")
#logger.info("Name widget1 " + str(name_widget1))
#logger.info("Name widget2 " + str(name_widget2))
name_pv1 = ScriptUtil.getPrimaryPV(name_widget1)
name_pv2 = ScriptUtil.getPrimaryPV(name_widget2)
#logger.info("Name PV1 " + str(name_pv1))
#logger.info("Name PV2 " + str(name_pv2))
value1 = PVUtil.getLong(name_pv1)
value2 = PVUtil.getLong(name_pv2)
#logger.info("value1="+str(value1))
#logger.info("value2="+str(value2))
if value1 == 0:
	name_widget1.setPropertyValue("confirm_message", "Are your sure you want turn ON this gauge?")
	#logger.info("ON")
else:
	name_widget1.setPropertyValue("confirm_message", "Are your sure you want turn OFF this gauge?")
	#logger.info("OFF")

if value2 == 0:
	name_widget2.setPropertyValue("confirm_message", "Are your sure you want turn ON this gauge?")
	#logger.info("ON")
else:
	name_widget2.setPropertyValue("confirm_message", "Are your sure you want turn OFF this gauge?")
	#logger.info("OFF")
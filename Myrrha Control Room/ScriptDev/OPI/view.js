//import
importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);
importPackage(Packages.java.lang);
importPackage(Packages.org.eclipse.swt);
importPackage(Packages.java.io);

//This script set the position of DisplayOPI with the position of mouse

//get mouse position
mouse = java.awt.MouseInfo;
p = mouse.getPointerInfo();
value = p.getLocation();

//get button
device = display.getWidget("Action Button");

//balise xml <x>0</x> <y>0</y>
//create text with new position     
var textX= "<x>"+value.x+"</x>";
var textY= "<y>"+value.y+"</y>";

//read file
var filePath = "/Myrrha Control Room/epics-magnet-PS_V2/OPI/test.opi";
var oritext = FileUtil.readTextFile(filePath);

//replace first occurrence of position with the new position
var textin = oritext.replace("<x>0</x>", textX);
var textin = textin.replace("<y>0</y>", textY);

//Write new file
FileUtil.writeTextFile(filePath, true, textin, false); 

//close opi associated with device or/and open opi 
ScriptUtil.closeAssociatedOPI(device);
ScriptUtil.openOPI(display, "test.opi", 8, null);

//rewrite original file
FileUtil.writeTextFile(filePath, true, oritext, false);   

   
   
   
   
   
   
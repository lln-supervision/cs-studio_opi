importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var wfLong = PVUtil.getLongArray(pvArray[0]);
text = "";
for (length = 0; wfLong[length] != 0; length++) {
	text += String.fromCharCode(wfLong[length]);
}

widget.setPropertyValue("pv_value",text);
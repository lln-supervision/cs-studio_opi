<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://exslt.org/strings">
	<!-- Identity template - copy everything -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/ED Direct To Direct.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/ED Direct To Direct.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/ED Direct To Direct.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/ED Direct To Direct.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/ED Direct To Direct.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/ED Direct To Direct.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TV Auto-Transformer.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TV Auto-Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TV Auto-Transformer.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Transformers/TV Auto-Transformer.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TV Auto-Transformer.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TV Auto-Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 2.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 2.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 2.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 2.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 2.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 2.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Magneto-Thermal Circuit Breaker On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Magneto-Thermal Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Magneto-Thermal Circuit Breaker On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Magneto-Thermal Circuit Breaker On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Magneto-Thermal Circuit Breaker On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Magneto-Thermal Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP General Transformer.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP General Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP General Transformer.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Transformers/TP General Transformer.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP General Transformer.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP General Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Resistor.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Resistor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Resistor.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Resistor.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Resistor.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Resistor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse Switch Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse Switch Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse Switch Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse Switch Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Wye Delta.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Wye Delta.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Wye Delta.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Starters/GS Wye Delta.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Wye Delta.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Wye Delta.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Coil.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Coil.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Coil.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Coil.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Magneto-Thermal Circuit Breaker Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Magneto-Thermal Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Magneto-Thermal Circuit Breaker Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Magneto-Thermal Circuit Breaker Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Magneto-Thermal Circuit Breaker Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Magneto-Thermal Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 3.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 3.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 3.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 3.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 3.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 3.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CE Turbo Alternator.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/CE Turbo Alternator.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CE Turbo Alternator.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Multidiscipline_Symbols/CE Turbo Alternator.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CE Turbo Alternator.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/CE Turbo Alternator.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/BJ Junction Box.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/BJ Junction Box.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/BJ Junction Box.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Enclosures/BJ Junction Box.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/BJ Junction Box.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/BJ Junction Box.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Rectifier in Full Wave Bridge Connection.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Rectifier in Full Wave Bridge Connection.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Rectifier in Full Wave Bridge Connection.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/CV Rectifier in Full Wave Bridge Connection.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Rectifier in Full Wave Bridge Connection.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Rectifier in Full Wave Bridge Connection.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Derivation Excitation Coil.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Derivation Excitation Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Derivation Excitation Coil.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Derivation Excitation Coil.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Derivation Excitation Coil.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Derivation Excitation Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Heating Element.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Heating Element.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Heating Element.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Heating Element.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Heating Element.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Heating Element.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Power Factor Meter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Power Factor Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Power Factor Meter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Power Factor Meter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Power Factor Meter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Power Factor Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 0.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 0.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 0.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 0.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 0.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 0.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/VG Motorized Valve.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/VG Motorized Valve.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/VG Motorized Valve.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Multidiscipline_Symbols/VG Motorized Valve.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/VG Motorized Valve.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/VG Motorized Valve.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Inertia Wheel.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Inertia Wheel.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Inertia Wheel.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Inertia Wheel.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Inertia Wheel.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Inertia Wheel.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Star Connection.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Star Connection.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Star Connection.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Star Connection.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Star Connection.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Star Connection.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Thyristor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/CV Thyristor.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Thyristor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/VG 2 way-Valves On-Off Valve.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/VG 2 way-Valves On-Off Valve.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/VG 2 way-Valves On-Off Valve.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Multidiscipline_Symbols/VG 2 way-Valves On-Off Valve.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/VG 2 way-Valves On-Off Valve.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/VG 2 way-Valves On-Off Valve.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Earth.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Earth.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Earth.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Earth.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Earth.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Earth.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Volt Ampere Meter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Volt Ampere Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Volt Ampere Meter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Volt Ampere Meter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Volt Ampere Meter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Volt Ampere Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Circuit Breaker On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Circuit Breaker On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Circuit Breaker On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Circuit Breaker On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Ampere Hour.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Ampere Hour.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Ampere Hour.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Ampere Hour.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Ampere Hour.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Ampere Hour.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frequency Meter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Frequency Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frequency Meter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Frequency Meter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frequency Meter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Frequency Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CD Diode.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CD Diode.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CD Diode.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/CD Diode.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CD Diode.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CD Diode.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CU Box.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/CU Box.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CU Box.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Enclosures/CU Box.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CU Box.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/CU Box.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Disconnecting.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Disconnecting.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Disconnecting.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Disconnecting.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Disconnecting.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Disconnecting.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM General.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GM General.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM General.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Rotating_Machines/GM General.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM General.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GM General.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Switch Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Switch Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Switch Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Switch Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor Valve.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Thyristor Valve.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor Valve.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/CV Thyristor Valve.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor Valve.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Thyristor Valve.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Wattmeter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Wattmeter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Wattmeter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Wattmeter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Wattmeter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Wattmeter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Circuit Breaker On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Circuit Breaker On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Circuit Breaker On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Circuit Breaker On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JR Lightning Arrester.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/JR Lightning Arrester.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JR Lightning Arrester.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/JR Lightning Arrester.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JR Lightning Arrester.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/JR Lightning Arrester.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/XM Electromagnetic Effect.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/XM Electromagnetic Effect.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/XM Electromagnetic Effect.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/XM Electromagnetic Effect.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/XM Electromagnetic Effect.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/XM Electromagnetic Effect.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Extended Delta.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Extended Delta.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Extended Delta.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Extended Delta.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Extended Delta.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Extended Delta.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frame.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Frame.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frame.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Frame.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frame.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Frame.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Switch Disconnector on Isolating switch Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Switch Disconnector on Isolating switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Switch Disconnector on Isolating switch Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Switch Disconnector on Isolating switch Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Switch Disconnector on Isolating switch Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Switch Disconnector on Isolating switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Watt-Hour Meter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Watt-Hour Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Watt-Hour Meter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Watt-Hour Meter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Watt-Hour Meter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Watt-Hour Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Disconnecting Switch On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Disconnecting Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Disconnecting Switch On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Disconnecting Switch On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Disconnecting Switch On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Disconnecting Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Withdrawable Fuse Switch Disconnector Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Fuse Switch Disconnector Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Withdrawable Fuse Switch Disconnector Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Fuse Switch Disconnector Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Withdrawable Fuse Switch Disconnector Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Fuse Switch Disconnector Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Break Contact.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Break Contact.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Break Contact.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Break Contact.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Break Contact.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Break Contact.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Zigzag Connection with Neutral brought out.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Zigzag Connection with Neutral brought out.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Zigzag Connection with Neutral brought out.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Zigzag Connection with Neutral brought out.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Zigzag Connection with Neutral brought out.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Zigzag Connection with Neutral brought out.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Schedule.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Schedule.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Schedule.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Schedule.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Schedule.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Schedule.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Circuit Breaker Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Circuit Breaker Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Circuit Breaker Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Circuit Breaker Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Brush.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Brush.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Brush.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Brush.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Brush.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Brush.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Withdrawable Fuse Switch Disconnector On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Fuse Switch Disconnector On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Withdrawable Fuse Switch Disconnector On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Fuse Switch Disconnector On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Withdrawable Fuse Switch Disconnector On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Fuse Switch Disconnector On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JC Contactor Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Contactor Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JC Contactor Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Contactor Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JC Contactor Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Contactor Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JB Busbar.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/JB Busbar.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JB Busbar.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/JB Busbar.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JB Busbar.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/JB Busbar.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Switch Disconnector on Isolating switch On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Switch Disconnector on Isolating switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Switch Disconnector on Isolating switch On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Switch Disconnector on Isolating switch On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Switch Disconnector on Isolating switch On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Switch Disconnector on Isolating switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse Switch On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse Switch On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse Switch On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse Switch On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/General Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA General Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/General Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA General Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/General Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA General Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JB Isolated Busbar.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/JB Isolated Busbar.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JB Isolated Busbar.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/JB Isolated Busbar.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JB Isolated Busbar.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/JB Isolated Busbar.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 1.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 1.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 1.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 1.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 1.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 1.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Transformer with Three Windings with Earthed Screen.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP Transformer with Three Windings with Earthed Screen.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Transformer with Three Windings with Earthed Screen.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Transformers/TP Transformer with Three Windings with Earthed Screen.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Transformer with Three Windings with Earthed Screen.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP Transformer with Three Windings with Earthed Screen.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Permanent Magnet.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Permanent Magnet.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Permanent Magnet.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Permanent Magnet.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Permanent Magnet.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Permanent Magnet.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Closing Push-Button On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Closing Push-Button On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Closing Push-Button On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Closing Push-Button On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Closing Push-Button On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Closing Push-Button On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JU Combined Disconnector and Earthing Switch Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Combined Disconnector and Earthing Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JU Combined Disconnector and Earthing Switch Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Combined Disconnector and Earthing Switch Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JU Combined Disconnector and Earthing Switch Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Combined Disconnector and Earthing Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Emergency Light.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Emergency Light.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Emergency Light.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Emergency Light.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Emergency Light.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Emergency Light.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Switch On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Switch On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Switch On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Switch On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EB n Elements.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Accumulators/EB n Elements.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EB n Elements.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Accumulators/EB n Elements.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EB n Elements.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Accumulators/EB n Elements.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 1.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 1.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 1.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 1.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 3-Positions Switch 1.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 3-Positions Switch 1.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Differential Circuit Breaker Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Differential Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Differential Circuit Breaker Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Differential Circuit Breaker Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Differential Circuit Breaker Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Differential Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/SM 2-Positions Switch 1.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 2-Positions Switch 1.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/SM 2-Positions Switch 1.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 2-Positions Switch 1.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/SM 2-Positions Switch 1.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 2-Positions Switch 1.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter-Regulator.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Starter-Regulator.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter-Regulator.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Starters/GS Starter-Regulator.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter-Regulator.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Starter-Regulator.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Ammeter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Ammeter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Ammeter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Ammeter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Ammeter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Ammeter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/SM 2-Positions Switch 0.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 2-Positions Switch 0.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/SM 2-Positions Switch 0.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 2-Positions Switch 0.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/SM 2-Positions Switch 0.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 2-Positions Switch 0.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG General Converter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG General Converter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG General Converter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/EG General Converter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG General Converter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG General Converter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Circuit Breaker Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Circuit Breaker Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Circuit Breaker Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Disconnecting Circuit Breaker Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Disconnecting Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EB General.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Accumulators/EB General.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EB General.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Accumulators/EB General.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EB General.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Accumulators/EB General.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 0.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 0.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 0.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 0.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 0.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 0.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor Bridge.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Thyristor Bridge.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor Bridge.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/CV Thyristor Bridge.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CV Thyristor Bridge.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/CV Thyristor Bridge.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM Direct.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GM Direct.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM Direct.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Rotating_Machines/GM Direct.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM Direct.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GM Direct.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TV Transformer with Three Windings.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TV Transformer with Three Windings.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TV Transformer with Three Windings.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Transformers/TV Transformer with Three Windings.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TV Transformer with Three Windings.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TV Transformer with Three Windings.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM Alternating, Three Phase.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GM Alternating, Three Phase.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM Alternating, Three Phase.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Rotating_Machines/GM Alternating, Three Phase.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GM Alternating, Three Phase.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GM Alternating, Three Phase.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JC Contactor On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Contactor On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JC Contactor On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Contactor On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JC Contactor On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Contactor On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Withdrawable Fuse Contactor On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Fuse Contactor On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Withdrawable Fuse Contactor On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Fuse Contactor On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Withdrawable Fuse Contactor On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Fuse Contactor On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Bypass.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG Bypass.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Bypass.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/EG Bypass.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Bypass.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG Bypass.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Fuse.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JF Fuse.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Volt-Ampere reactive Hour meter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Volt-Ampere reactive Hour meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Volt-Ampere reactive Hour meter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Volt-Ampere reactive Hour meter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Volt-Ampere reactive Hour meter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Volt-Ampere reactive Hour meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JU Combined Disconnector and Earthing Switch On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Combined Disconnector and Earthing Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JU Combined Disconnector and Earthing Switch On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Combined Disconnector and Earthing Switch On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JU Combined Disconnector and Earthing Switch On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Combined Disconnector and Earthing Switch On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/PV GV Excitation Coil.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/PV GV Excitation Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/PV GV Excitation Coil.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/PV GV Excitation Coil.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/PV GV Excitation Coil.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/PV GV Excitation Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/+ Sign.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/+ Sign.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/+ Sign.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/+ Sign.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/+ Sign.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/+ Sign.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Circuit Breaker On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Circuit Breaker On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Circuit Breaker On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Circuit Breaker On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Withdrawable Fuse Contactor Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Fuse Contactor Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Withdrawable Fuse Contactor Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Fuse Contactor Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JA Withdrawable Fuse Contactor Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Fuse Contactor Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Step.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Step.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Step.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Starters/GS Step.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Step.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Step.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Differential Circuit Breaker On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Differential Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Differential Circuit Breaker On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Differential Circuit Breaker On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JS Differential Circuit Breaker On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Differential Circuit Breaker On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Synchronoscope.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Synchronoscope.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Synchronoscope.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Synchronoscope.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Synchronoscope.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Synchronoscope.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Voltmeter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Voltmeter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Voltmeter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Voltmeter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Voltmeter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Voltmeter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Multiple Coils Transformer.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP Multiple Coils Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Multiple Coils Transformer.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Transformers/TP Multiple Coils Transformer.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Multiple Coils Transformer.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP Multiple Coils Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Closing Push-Button Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Closing Push-Button Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Closing Push-Button Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Closing Push-Button Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Closing Push-Button Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Closing Push-Button Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Disconnecting Switch Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Disconnecting Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Disconnecting Switch Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Disconnecting Switch Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Disconnecting Switch Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Withdrawable Disconnecting Switch Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Reactor.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Reactor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Reactor.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Reactor.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Reactor.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Reactor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CB Capacitor.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/CB Capacitor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CB Capacitor.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/CB Capacitor.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CB Capacitor.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/CB Capacitor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Mechanical Interlock.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Mechanical Interlock.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Mechanical Interlock.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Mechanical Interlock.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Mechanical Interlock.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Mechanical Interlock.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Locking.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Locking.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Locking.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Locking.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Locking.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Locking.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 2.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 2.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 2.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 2.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JM 4-Positions Switch 2.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/SM 4-Positions Switch 2.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Make Contact.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Make Contact.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Make Contact.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Make Contact.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Make Contact.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Make Contact.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Lamp.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Lamp.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Lamp.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Lamp.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Lamp.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Lamp.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/PL Pump.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/PL Pump.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/PL Pump.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Multidiscipline_Symbols/PL Pump.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/PL Pump.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Multidiscipline_Symbols/PL Pump.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Delta Connection.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Delta Connection.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Delta Connection.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Delta Connection.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Delta Connection.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Delta Connection.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Phase Meter.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Phase Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Phase Meter.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Phase Meter.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Phase Meter.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Phase Meter.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Socket.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Socket.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Socket.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Socket.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Socket.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Socket.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/General On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA General On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/General On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA General On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/General On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA General On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Neutral Point.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Neutral Point.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Neutral Point.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Neutral Point.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Neutral Point.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Neutral Point.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CU Cabinet.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/CU Cabinet.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CU Cabinet.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Enclosures/CU Cabinet.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CU Cabinet.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/CU Cabinet.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Transformer with Earthed Screen.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP Transformer with Earthed Screen.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Transformer with Earthed Screen.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Transformers/TP Transformer with Earthed Screen.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TP Transformer with Earthed Screen.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TP Transformer with Earthed Screen.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter Direct-on-line.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Starter Direct-on-line.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter Direct-on-line.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Starters/GS Starter Direct-on-line.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter Direct-on-line.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Starter Direct-on-line.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Fuse Switch Disconnector On.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Fuse Switch Disconnector On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Fuse Switch Disconnector On.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Fuse Switch Disconnector On.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Fuse Switch Disconnector On.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Fuse Switch Disconnector On.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Relay Coil.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Relay Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Relay Coil.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Relay Coil.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Relay Coil.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Relay Coil.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Direct To Alternating.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG Direct To Alternating.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Direct To Alternating.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/EG Direct To Alternating.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Direct To Alternating.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG Direct To Alternating.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GE Diesel Alternator.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GE Diesel Alternator.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GE Diesel Alternator.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Rotating_Machines/GE Diesel Alternator.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GE Diesel Alternator.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Rotating_Machines/GE Diesel Alternator.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/- Sign.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/- Sign.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/- Sign.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/- Sign.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/- Sign.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/- Sign.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter-Regulator with Thyristors.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Starter-Regulator with Thyristors.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter-Regulator with Thyristors.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Starters/GS Starter-Regulator with Thyristors.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Starter-Regulator with Thyristors.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Starter-Regulator with Thyristors.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Impedance.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Impedance.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Impedance.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/Impedance.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Impedance.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/Impedance.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CC Console.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/CC Console.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CC Console.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Enclosures/CC Console.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/CC Console.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Enclosures/CC Console.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EQ Power Factor Capacitor.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/EQ Power Factor Capacitor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EQ Power Factor Capacitor.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Miscellaneous/EQ Power Factor Capacitor.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EQ Power Factor Capacitor.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Miscellaneous/EQ Power Factor Capacitor.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frequency.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Frequency.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frequency.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Measuring_Instruments_and_Sensors/Frequency.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Frequency.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Measuring_Instruments_and_Sensors/Frequency.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TC Current transformer.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TC Current transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TC Current transformer.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Transformers/TC Current transformer.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/TC Current transformer.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Transformers/TC Current transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Alternating To Direct.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG Alternating To Direct.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Alternating To Direct.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Converters/EG Alternating To Direct.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/EG Alternating To Direct.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Converters/EG Alternating To Direct.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/XT Thermal Effect.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/XT Thermal Effect.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/XT Thermal Effect.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/XT Thermal Effect.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/XT Thermal Effect.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/XT Thermal Effect.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Auto-Transformer.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Auto-Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Auto-Transformer.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Starters/GS Auto-Transformer.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Auto-Transformer.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS Auto-Transformer.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Circuit Breaker Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Circuit Breaker Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Circuit Breaker Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/JF Withdrawable Circuit Breaker Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JA Withdrawable Circuit Breaker Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GS General.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS General.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GS General.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Starters/GS General.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/GS General.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Starters/GS General.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Fuse Switch Disconnector Off.svg']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Fuse Switch Disconnector Off.svg</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Fuse Switch Disconnector Off.png']">
		<xsl:element name="image_file">SymbolLibrary/PNG/Electrical/Protection_and_Cut-Shut_Off-On/JS Fuse Switch Disconnector Off.png</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[. = 'SymbolLibrary/Fuse Switch Disconnector Off.gif']">
		<xsl:element name="image_file">SymbolLibrary/SVG/Electrical/Protection_and_Cut-Shut_Off-On/JS Fuse Switch Disconnector Off.svg</xsl:element>
	</xsl:template>
</xsl:stylesheet>
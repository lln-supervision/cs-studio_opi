<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://exslt.org/strings">
	<!-- Identity template - copy everything -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- Change-specific part -->
	<xsl:template match="@typeId[starts-with(., 'org.csstudio.plugin.symbol.')]">
		<xsl:attribute name="typeId">
			<xsl:value-of select="str:replace(., 'org.csstudio.plugin.symbol.', 'org.csstudio.opibuilder.widgets.symbol.')"/>
		</xsl:attribute>
	</xsl:template>
	<xsl:template match="@typeId[starts-with(., 'org.csstudio.opibuilder.boy.symbol.')]">
		<xsl:attribute name="typeId">
			<xsl:value-of select="str:replace(., 'org.csstudio.opibuilder.boy.symbol.', 'org.csstudio.opibuilder.widgets.symbol.')"/>
		</xsl:attribute>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Widthdrawable Fuse Switch Disconnector')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Widthdrawable Fuse Switch Disconnector', 'Withdrawable Fuse Switch Disconnector')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '/On-Off Valve')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '/On-Off Valve', '/VG 2 way-Valves On-Off Valve')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Switch Disconnector, on Isolating switch')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Switch Disconnector, on Isolating switch', 'Switch Disconnector on Isolating switch')"/>
		</xsl:element>
	</xsl:template>
	<!-- ... -->
</xsl:stylesheet>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://exslt.org/strings">
	<!-- Identity template - copy everything -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JM 3-Positions Switch 2')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JM 3-Positions Switch 2', 'SM 3-Positions Switch 2')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Wye Delta')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Wye Delta', 'GS Wye Delta')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JM 4-Positions Switch 3')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JM 4-Positions Switch 3', 'SM 4-Positions Switch 3')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JM 3-Positions Switch 0')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JM 3-Positions Switch 0', 'SM 3-Positions Switch 0')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '3 way-Valves Control Automatic Recirculation Valve')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '3 way-Valves Control Automatic Recirculation Valve', 'VC 3 way-Valves Control Automatic Recirculation Valve')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '3 way-Valves On-Off Ball Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '3 way-Valves On-Off Ball Construction', 'VG 3 way-Valves On-Off Ball Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JF Withdrawable Circuit Breaker On')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JF Withdrawable Circuit Breaker On', 'JA Withdrawable Circuit Breaker On')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '3 way-Valves Control Angle Relief Globe Valve')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '3 way-Valves Control Angle Relief Globe Valve', 'VC 3 way-Valves Control Angle Relief Globe Valve')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '2 way-Valves Control Needle Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '2 way-Valves Control Needle Construction', 'VC 2 way-Valves Control Needle Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JF Withdrawable Disconnecting Switch On')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JF Withdrawable Disconnecting Switch On', 'JS Withdrawable Disconnecting Switch On')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '3 way-Valves Control Flow Balancing Valve')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '3 way-Valves Control Flow Balancing Valve', 'VC 3 way-Valves Control Flow Balancing Valve')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JC Contactor Off')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JC Contactor Off', 'JA Contactor Off')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '2 way-Valves Control Butterfly Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '2 way-Valves Control Butterfly Construction', 'VC 2 way-Valves Control Butterfly Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'General Off')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'General Off', 'JA General Off')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JM 4-Positions Switch 1')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JM 4-Positions Switch 1', 'SM 4-Positions Switch 1')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JU Combined Disconnector and Earthing Switch Off')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JU Combined Disconnector and Earthing Switch Off', 'JS Combined Disconnector and Earthing Switch Off')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JM 3-Positions Switch 1')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JM 3-Positions Switch 1', 'SM 3-Positions Switch 1')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '2 way-Valves Control Globe Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '2 way-Valves Control Globe Construction', 'VC 2 way-Valves Control Globe Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JS Differential Circuit Breaker Off')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JS Differential Circuit Breaker Off', 'JA Differential Circuit Breaker Off')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Starter-Regulator')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Starter-Regulator', 'GS Starter-Regulator')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JM 4-Positions Switch 0')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JM 4-Positions Switch 0', 'SM 4-Positions Switch 0')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'General Penetration')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'General Penetration', 'TW General Penetration')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JC Contactor On')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JC Contactor On', 'JA Contactor On')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '2 way-Valves Control Diaphragm Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '2 way-Valves Control Diaphragm Construction', 'VC 2 way-Valves Control Diaphragm Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JU Combined Disconnector and Earthing Switch On')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JU Combined Disconnector and Earthing Switch On', 'JS Combined Disconnector and Earthing Switch On')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Step')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Step', 'GS Step')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JS Differential Circuit Breaker On')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JS Differential Circuit Breaker On', 'JA Differential Circuit Breaker On')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JF Withdrawable Disconnecting Switch Off')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JF Withdrawable Disconnecting Switch Off', 'JS Withdrawable Disconnecting Switch Off')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '2 way-Valves Control Ball Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '2 way-Valves Control Ball Construction', 'VC 2 way-Valves Control Ball Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JM 4-Positions Switch 2')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JM 4-Positions Switch 2', 'SM 4-Positions Switch 2')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Relief Devices')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Relief Devices', 'VR Relief Devices')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '2 way-Valves Control Plug Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '2 way-Valves Control Plug Construction', 'VC 2 way-Valves Control Plug Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'General On')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'General On', 'JA General On')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Starter Direct-on-line')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Starter Direct-on-line', 'GS Starter Direct-on-line')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Fuse Switch Disconnector On')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Fuse Switch Disconnector On', 'JS Fuse Switch Disconnector On')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Starter-Regulator with Thyristors')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Starter-Regulator with Thyristors', 'GS Starter-Regulator with Thyristors')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '2 way-Valves Control Gate Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '2 way-Valves Control Gate Construction', 'VC 2 way-Valves Control Gate Construction')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Auto-Transformer')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Auto-Transformer', 'GS Auto-Transformer')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'JF Withdrawable Circuit Breaker Off')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'JF Withdrawable Circuit Breaker Off', 'JA Withdrawable Circuit Breaker Off')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., 'Fuse Switch Disconnector Off')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., 'Fuse Switch Disconnector Off', 'JS Fuse Switch Disconnector Off')"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="image_file[contains(., '3 way-Valves On-Off Glob Construction')]">
		<xsl:element name="image_file">
			<xsl:value-of select="str:replace(., '3 way-Valves On-Off Glob Construction', 'VG 3 way-Valves On-Off Glob Construction')"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
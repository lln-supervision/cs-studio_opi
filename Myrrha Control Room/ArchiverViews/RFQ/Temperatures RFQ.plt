<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title>Temperatures RFQ</title>
    <save_changes>true</save_changes>
    <show_legend>true</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>false</grid>
    <scroll>true</scroll>
    <update_period>3.0</update_period>
    <scroll_step>5</scroll_step>
    <start>-16.00 h</start>
    <end>now</end>
    <archive_rescale>NONE</archive_rescale>
    <foreground>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
    </foreground>
    <background>
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
    </background>
    <title_font>Cantarell|16|1</title_font>
    <label_font>Cantarell|11|1</label_font>
    <scale_font>Cantarell|10|0</scale_font>
    <legend_font>Cantarell|10|0</legend_font>
    <axes>
        <axis>
            <visible>true</visible>
            <name>Temperatures (&#176;C)</name>
            <use_axis_name>true</use_axis_name>
            <use_trace_names>false</use_trace_names>
            <right>true</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>19.2185</min>
            <max>29.811500000000002</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>RFQ:MPS:Temp:FP2</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:FP2</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:E-Mean</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:E-Mean</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:TP-Mean</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:TP-Mean</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:FP1</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:FP1</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>128</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:TP-Max</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:TP-Max</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>128</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:CL</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:CL</name>
            <axis>0</axis>
            <color>
                <red>128</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:E-Max</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:E-Max</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:Tuner</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:Tuner</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:Inlet</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:Inlet</name>
            <axis>0</axis>
            <color>
                <red>128</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>RFQ:MPS:Temp:Abs-Max</display_name>
            <visible>true</visible>
            <name>RFQ:MPS:Temp:Abs-Max</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>128</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>Myrrha Archiver</name>
                <url>pbraw://archiver.myrrha.lan/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
    </pvlist>
</databrowser>
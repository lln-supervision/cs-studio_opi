//import
PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

//This script set the position of DisplayBOB with the position of mouse

//get mouse position
mouse = java.awt.MouseInfo;
p = mouse.getPointerInfo();
value = p.getLocation();

//set macro and set windows position
display = widget.getDisplayModel();
name = display.getPropertyValue("name");
display.getPropertyValue("macros").add("posX", value.x);
display.getPropertyValue("macros").add("posY", value.y);

//Select correct detailview
if(name.contains("TMP_AV.bob")){
ScriptUtil.openDisplay(display,"../opi/TMP_Detail.bob","STANDALONE", null);
}
else{
ScriptUtil.openDisplay(display,"../opi/IGP_Detail.bob","STANDALONE", null);
}

//debug
//logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
//logger.info("test start");
//logger.info("name= "+ name);
//logger.info("valueX= " + value.x);
//logger.info("valueY= " + value.y);
//logger.info("test stop");



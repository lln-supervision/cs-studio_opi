importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);

PVarray=DataUtil.createDoubleArray(2);
PVarray[0] = PVUtil.getString(pvs[0]);
PVarray[1] = PVUtil.getString(pvs[1]);

var property_start = "trace_";
var property_end = "_visible";

for (var i=0;i<2;i++)
{
	if ( (PVarray[i])=="0")
	{
		widget.setPropertyValue((property_start.concat(i)).concat(property_end),"FALSE");
	}
	else
	{
		widget.setPropertyValue((property_start.concat(i)).concat(property_end),"TRUE");
	}
}




importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);

PVarray=DataUtil.createDoubleArray(10);
PVarray[0] = PVUtil.getString(pvs[0]);
PVarray[1] = PVUtil.getString(pvs[1]);
PVarray[2] = PVUtil.getString(pvs[2]);
PVarray[3] = PVUtil.getString(pvs[3]);
PVarray[4] = PVUtil.getString(pvs[4]);
PVarray[5] = PVUtil.getString(pvs[5]);
PVarray[6] = PVUtil.getString(pvs[6]);
PVarray[7] = PVUtil.getString(pvs[7]);
PVarray[8] = PVUtil.getString(pvs[8]);
PVarray[9] = PVUtil.getString(pvs[9]);

var property_start = "trace_";
var property_end = "_visible";

for (var i=0;i<10;i++)
{
	if ( (PVarray[i])=="0")
	{
		widget.setPropertyValue((property_start.concat(i)).concat(property_end),"FALSE");
	}
	else
	{
		widget.setPropertyValue((property_start.concat(i)).concat(property_end),"TRUE");
	}
}




importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.opibuilder.datatUtil);

var channel = PVUtil.getDoubleArray(pvs[0]);
var y = PVUtil.getDoubleArray(pvs[2]);
var x = PVUtil.getDoubleArray(pvs[3]);

var len = PVUtil.getLong(pvs[1]);

var trace_num = PVUtil.getLong(pvs[4]);



//var buffer_length = widget.getPropertyValue("trace_"+trace_num+"_buffer_size");
var buffer_length = 3000;


if((x.length+len/2 > buffer_length)&&(buffer_length>len/2)) {
	x = x.splice(x.length-buffer_length+len/2);
	y = y.splice(y.length-buffer_length+len/2);
}
else if ((x.length+len/2 > buffer_length)&&(buffer_length<len/2)){
	x = [];
	y = [];
	ConsoleUtil.writeInfo("Buffer smaller than amount of data received! Not all of the data was displayed on the graph.");
}


x = x.concat(channel.slice(0,len/2));
y = y.concat(channel.slice(len/2,len));


pvs[2].setValue(DataUtil.toJavaDoubleArray(y));
pvs[3].setValue(DataUtil.toJavaDoubleArray(x));

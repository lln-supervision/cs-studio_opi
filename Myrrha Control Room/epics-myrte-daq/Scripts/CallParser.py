from org.csstudio.opibuilder.scriptUtil import PVUtil
from org.csstudio.opibuilder.scriptUtil import FileUtil
from org.csstudio.opibuilder.scriptUtil import ConsoleUtil

from java.lang import Runtime
from java.lang import Process
from java.lang import String
from jarray import array

#if PVUtil.getLong(pvs[0]) == 1:

    #Put the correct file path in the string below (ex. "/media/sfshare/")
    #filePath = "" + PVUtil.getString(pvs[1])
    #filePath = FileUtil.openFileDialog(False)
    #ConsoleUtil.writeInfo(filePath);

    #The "/home/osboxes/workspace/CSS/DAQ_EPICS_GUI" might need to be changed
    #parserPath = "/home/dev/CSS-Workspaces/Default/MYRTE_DAQ/Scripts/Parser.py"

    #if filePath:
        #cmd = "python "+parserPath+" "+filePath

        #runtime = Runtime.getRuntime()

        #p = runtime.exec(cmd)

        #p.waitFor()

filePath = FileUtil.openFileDialog(False)
parserPath = "/opt/codac-4.1/opi/boy/epics-myrte-daq/Scripts/Parser.py"
if filePath:
    cmd = "python "+parserPath+" "+filePath
    runtime = Runtime.getRuntime()
    p = runtime.exec(cmd)
    p.waitFor()

import sys
import os

filePath = sys.argv[1]


#Enter the correct IP:
os.environ["EPICS_CA_ADDR_LIST"] = "10.5.3.90"

replaceTable = [["ACQUISITION NAME", "acqName"],
["TRIGGER MODE", "trigMode"],
["TRIGGER SOURCE", "trigSource"],
["SAMPLING FREQUENCY [HZ]", "samplingFrequency"],
["MEASUREMENTS PER WAVEFORM", "measurementsNumber"],
["ACTIVE CHANNELS", "channelActive"],
["SAMPLES PER MEASURMENT", "samplesNumber"],
["DISPLAY TIME DELAY", "displayDelay"],
["INPUT VOLTAGE RANGE", "voltageRange"],
["DISPLAY DECIMATION", "decimation"]]

replaceChannels = [" - CHANNEL 1", " - CHANNEL 2", " - CHANNEL 3", " - CHANNEL 4", " - CHANNEL 5",
" - CHANNEL 6", " - CHANNEL 7", " - CHANNEL 8", " - CHANNEL 9", " - CHANNEL 10", " - CHANNEL 11",
" - CHANNEL 12", " - CHANNEL 13", " - CHANNEL 14", " - CHANNEL 15", " - CHANNEL 16"]

replaceVoltage = [["-1V..1V", "1"], ["-2V..2V", "2"], ["-5V..5V","3"], ["-10V..10V","4"]]

replaceMode = [["Continious", "0"], ["Triggered", "1"]]

replaceSource = [["Software", "0"], ["Hardware", "1"]]

#The "/home/codac-dev/workspace/CSS/CSS/DAQ_EPICS_GUI" might need to be changed
templateFile = "/opt/codac-4.1/opi/boy/epics-myrte-daq/Scripts/recordsTemplate.txt"
#create a 2-D list containing all record names and default values, from a template file
f = open(templateFile, "r")
records = f.read()
f.close()
records = records.split("\n")
temp = []
for line in records:
    line = line.split(" ")
    temp.append(line)
records = temp
records = [x for x in records if x[0]]


#read whole file
f = open(filePath, "r")
tfs = f.read()
f.close()

#split the file by lines
tfs = tfs.split("\n")


#insert record names instead of descriptions
temp = []
for line in tfs:
    for record in replaceTable:
        if line.find(record[0]) == -1:
            continue
        temp.append(line.replace(record[0], record[1]))
tfs = temp

for channel in replaceChannels:
    tfs = [line.replace(channel,str(replaceChannels.index(channel)+1)) for line in tfs]

#expand channel active and replace string values(description values) with int values
temp = []
for line in tfs:
    if line.find("channelActive") != -1:
        channels = line[line.index("\"")+1:line.index("\"",line.index("\"")+1)]#gets the string between the "..."
        channels = channels.split(" ")
        for channel in channels:
            temp.append("channelActive" + channel + " %    1")#1 stands for "on"
    elif line.find("voltageRange") != -1:
        voltage = line[line.index("\"")+1:line.index("\"",line.index("\"")+1)]#gets the string between the "..."
        for voltageRange in replaceVoltage:
            if voltage == voltageRange[0]:
                temp.append(line.replace("\""+voltage+"\"",voltageRange[1]))
    elif line.find("trigMode") != -1:
        mode = line[line.index("\"")+1:line.index("\"",line.index("\"")+1)]#gets the string between the "..."
        for trigMode in replaceMode:
            if mode == trigMode[0]:
                temp.append(line.replace("\""+mode+"\"",trigMode[1]))
    elif line.find("trigSource") != -1:
        source = line[line.index("\"")+1:line.index("\"",line.index("\"")+1)]#gets the string between the "..."
        for trigSource in replaceSource:
            if source == trigSource[0]:
                temp.append(line.replace("\""+source+"\"",trigSource[1]))  
    else:
        temp.append(line)
tfs = temp  


#final configuration in 2-D list
finalConfig = []
for line in tfs:
    line = line[:line.index("%")] + line[line.index("%")+5:]
    line = line.replace("@", "")
    line = line.replace("\"", "")
    if line.find("[") != -1:
        line = line[:line.index("[")] + line[line.index("]")+1:]
    line = line.split()
    finalConfig.append(line)

for record in records:
    flag = 0
    for config in finalConfig:
        if record[0] == config[0]:
            os.system("caput -n MYRRHA_DAQ:"+record[0]+" "+config[1])
            #caput(record[0], config[1])
            flag += 1
    if flag == 0:
        os.system("caput -n MYRRHA_DAQ:"+record[0]+" "+record[1])
        #caput(record[0], record[1])


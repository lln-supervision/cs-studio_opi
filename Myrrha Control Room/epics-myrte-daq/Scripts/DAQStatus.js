importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var newlog = PVUtil.getString(pvs[0]);

var timestamp = PVUtil.getString(pvs[1]);

var logs = widget.getPropertyValue("text");

if (timestamp.length() != 0){
	if (logs.length() > 20000){
		logs = logs.slice(logs.indexOf("[", 2));
	}
	
	logs = "[" + timestamp +"]: " + newlog + "\n" + logs;
}

widget.setPropertyValue("text", logs);
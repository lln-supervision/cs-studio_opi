importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
var channelSelected = PVUtil.getDouble(pvs[0]);

if (channelSelected == 1){
	widget.setPropertyValue("enabled", true);
	widget.setPropertyValue("foreground_color", ColorFontUtil.getColorFromRGB(255,255,255));
}
else{
	widget.setPropertyValue("enabled", false);
	widget.setPropertyValue("foreground_color", ColorFontUtil.getColorFromRGB(121,123,126));
}

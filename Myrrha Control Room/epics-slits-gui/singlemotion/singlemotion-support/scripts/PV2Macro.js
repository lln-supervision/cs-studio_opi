importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);
var pv1 = PVUtil.getString(pvArray[0]);
var pv2 = PVUtil.getString(pvArray[1]);
pv1 = pv2 + ":" + pv1;
macroInput.put("PV2Macro", pv1);
macroInput.put("PV3Macro", pv2);
widgetController.setPropertyValue("macros", macroInput);
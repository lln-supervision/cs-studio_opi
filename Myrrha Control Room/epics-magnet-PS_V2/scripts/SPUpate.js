// the purpose of this script is to bypass the bug of the CEANels driver which require to SET the outputSP even if it's already set.
// here but juste read/write on the same pv


importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);
importPackage(Packages.java.lang);
importPackage(Packages.org.eclipse.jface.dialogs);


var outputSP=PVUtil.getString(pvs[0])
var status=   PVUtil.getString(pvs[1])
if(status=="On")
{
	PVUtil.writePV(pvs[0], outputSP);
}

importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);
importPackage(Packages.java.lang);
importPackage(Packages.org.eclipse.jface.dialogs);


//ConsoleUtil.writeInfo("---- START ----");

var pvUtile = PVUtil.getString(pvs[0]);
var arrayPVTMP = pvUtile.split(';');
var mod = widget.getMacroValue("MOD");

//ConsoleUtil.writeInfo("mod "+mod);
//ConsoleUtil.writeInfo("---- pvUtile ---->    " + pvUtile);

display.getWidget("LED01").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatFPSwitchRB");
display.getWidget("LED02").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatACVoltLimitRB");
display.getWidget("LED03").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatPRMalfunctionRB");
display.getWidget("LED04").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatPSMalfunctionRB");
display.getWidget("LED05").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatFanStopRB");
display.getWidget("LED06").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatTempHighRB");
display.getWidget("LED07").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatTempRB");
display.getWidget("LED08").setPropertyValue("pv_name", arrayPVTMP[0] + ":ModStatCommRB");

display.getWidget("Label_2").setPropertyValue("text", arrayPVTMP[1]);

//ConsoleUtil.writeInfo("---- END ----");

    
   
importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);
importPackage(Packages.java.lang);
importPackage(Packages.org.eclipse.jface.dialogs);
importPackage(Packages.org.eclipse)

//ConsoleUtil.writeInfo(widget.getMacroValue("DEVICE"));
var macrosInput = DataUtil.createMacrosInput(true);
var naming = widget.getMacroValue("DEVICE");
macrosInput.put("DEVICE", naming)


// **** chose OPI based on the naming *****************************************


//if (naming.contains("CAENELS"))
//{
//	ScriptUtil.openOPI(widget, "../epics-caenels/EasyDriver.opi",7,macrosInput);
//}
//else
//{
//	ScriptUtil.openOPI(widget, "./OPI/MagnetPS_Main.opi",7,macrosInput);	
//}	
// ****************************************************************************


// **** chose OPI based on a dedicated PV on the IOC

var pv = widget.getPV();
var type = PVUtil.getString(pv);
//ConsoleUtil.writeInfo(type);
if ( type.contains("CAENELS"))
{
//Selection CAENELS cabinet + position window OPI

//get mouse position
mouse = java.awt.MouseInfo;
p = mouse.getPointerInfo();
value = p.getLocation();

//get button
//device = display.getWidget("Action Button");

//balise xml <x>0</x> <y>0</y>
//create text with new position     
var textX= "<x>"+value.x+"</x>";
var textY= "<y>"+value.y+"</y>";

//read file
var filePath = "/Myrrha Control Room/epics-magnet-PS_V2/OPI/EasyDriver.opi";
var oritext = FileUtil.readTextFile(filePath);

//replace first occurrence of position with the new position
var textin = oritext.replace("<x>0</x>", textX);
var textin = textin.replace("<y>0</y>", textY);

//Write new file
FileUtil.writeTextFile(filePath, true, textin, false); 

//close opi associated with device or/and open opi 
//ScriptUtil.closeAssociatedOPI(device);
ScriptUtil.openOPI(widget, "../OPI/EasyDriver.opi", 8, macrosInput);

//rewrite original file
FileUtil.writeTextFile(filePath, true, oritext, false);  

//oldscript//ScriptUtil.openOPI(widget, "../OPI/EasyDriver.opi",7,macrosInput); 
}
else
{
// Selection Jema cabinet + position window OPI

//get mouse position
mouse = java.awt.MouseInfo;
p = mouse.getPointerInfo();
value = p.getLocation();

//get button
//device = display.getWidget("Action Button");

//balise xml <x>0</x> <y>0</y>
//create text with new position     
var textX= "<x>"+value.x+"</x>";
var textY= "<y>"+value.y+"</y>";

//read file
var filePath = "/Myrrha Control Room/epics-magnet-PS_V2/OPI/MagnetPS_JEMA2.opi";
var oritext = FileUtil.readTextFile(filePath);

//replace first occurrence of position with the new position
var textin = oritext.replace("<x>0</x>", textX);
var textin = textin.replace("<y>0</y>", textY);

//Write new file
FileUtil.writeTextFile(filePath, true, textin, false); 

//close opi associated with device or/and open opi 
//ScriptUtil.closeAssociatedOPI(device);
ScriptUtil.openOPI(widget, "../OPI/MagnetPS_JEMA2.opi", 8, macrosInput);

//rewrite original file
FileUtil.writeTextFile(filePath, true, oritext, false); 
//oldscript//ScriptUtil.openOPI(widget, "../OPI/MagnetPS_JEMA2.opi",7,macrosInput);
}
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
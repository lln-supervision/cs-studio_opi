importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.org.csstudio.platform.data);
importPackage(Packages.java.lang);
importPackage(Packages.org.eclipse.jface.dialogs);
importPackage(Packages.org.eclipse)

//ConsoleUtil.writeInfo("---- START ----");

//var namingDevice = widget.getMacroValue("DEVICE");
//var ledTS = widget.getMacroValue("DEVICE");

var device = display.getWidget("Label_124").getPropertyValue("text");
//ConsoleUtil.writeInfo("Device  : " + device);
var subStringDevice = device.replace("JEMA PS : ", "");
//ConsoleUtil.writeInfo(subStringDevice.toString());
var pv = display.getWidget("ledTS").getPropertyValue("pv_name");
//ConsoleUtil.writeInfo("pv  : " + pv);

switch(String(subStringDevice))
{               
        case "I1-CH01:DF-QU-01":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH01:DF-TS-1:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH01:DF-QU-02":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH01:DF-TS-2:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH02:DF-QU-01":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH02:DF-TS-1:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH02:DF-QU-02":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH02:DF-TS-2:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH03:DF-QU-01":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH03:DF-TS-1:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH03:DF-QU-02":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH03:DF-TS-2:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;

        case "I1-CH04:DF-QU-01":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH04:DF-TS-1:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH04:DF-QU-02":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH04:DF-TS-2:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;

        case "I1-CH05:DF-QU-01":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH05:DF-TS-1:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH05:DF-QU-02":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH05:DF-TS-2:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;

        case "I1-CH06:DF-QU-01":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH06:DF-TS-1:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH06:DF-QU-02":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH06:DF-TS-2:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;

        case "I1-CH07:DF-QU-01":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH07:DF-TS-1:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;
        case "I1-CH07:DF-QU-02":
            //ConsoleUtil.writeInfo("case  : " + subStringDevice);
            display.getWidget("ledTS").setPropertyValue("pv_name", "I1-CH07:DF-TS-2:Status");
            display.getWidget("ledTS").setPropertyValue("visible", true);
            display.getWidget("labelTS").setPropertyValue("visible", true);
        break;

       case "I1-ME1:DF-CORH-01": 
       case "I1-ME1:DF-CORV-01":
              //ConsoleUtil.writeInfo("case  : " + subStringDevice);
              display.getWidget("ledTS").setPropertyValue("pv_name", "I1-ME1:DF-TS-1:Status");
              display.getWidget("ledTS").setPropertyValue("visible", true);
              display.getWidget("labelTS").setPropertyValue("visible", true);
              break;
       case "I1-ME1:DF-CORH-02": 
       case "I1-ME1:DF-CORV-02":
              //ConsoleUtil.writeInfo("case  : " + subStringDevice);
              display.getWidget("ledTS").setPropertyValue("pv_name", "I1-ME1:DF-TS-5:Status");
              display.getWidget("ledTS").setPropertyValue("visible", true);
       display.getWidget("labelTS").setPropertyValue("visible", true);
              break;
  case "I1-ME1:DF-QU-01":
              //ConsoleUtil.writeInfo("case  : " + subStringDevice);
              display.getWidget("ledTS").setPropertyValue("pv_name", "I1-ME1:DF-TS-2:Status");
              display.getWidget("ledTS").setPropertyValue("visible", true);
       display.getWidget("labelTS").setPropertyValue("visible", true);
              break;
  case "I1-ME1:DF-QU-02":
             //ConsoleUtil.writeInfo("case  : " + subStringDevice);
              display.getWidget("ledTS").setPropertyValue("pv_name", "I1-ME1:DF-TS-3:Status");
              display.getWidget("ledTS").setPropertyValue("visible", true);
       display.getWidget("labelTS").setPropertyValue("visible", true);
              break;
  case "I1-ME1:DF-QU-03":
              //ConsoleUtil.writeInfo("case  : " + subStringDevice);
              display.getWidget("ledTS").setPropertyValue("pv_name", "I1-ME1:DF-TS-4:Status");
              display.getWidget("ledTS").setPropertyValue("visible", true);
       display.getWidget("labelTS").setPropertyValue("visible", true);
              break;
       default:
    //ConsoleUtil.writeInfo("BAD VALUE :"+ subStringDevice.toString());
    display.getWidget("ledTS").setPropertyValue("visible", false);
    display.getWidget("labelTS").setPropertyValue("visible", false);
}

//ConsoleUtil.writeInfo("---- END ----");
